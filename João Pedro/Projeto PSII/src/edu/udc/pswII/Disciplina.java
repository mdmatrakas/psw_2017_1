package edu.udc.pswII;

import java.io.Serializable;

public class Disciplina implements Serializable {

	private static final long serialVersionUID = -6457491023735907760L;
	
	private String nome;
	private String professor;
	private String horario;
	private int id;
	
	public Disciplina(int id, String nome, String professor, String horario) {
		super();
		this.id = id;
		this.nome = nome;
		this.professor = professor;
		this.horario = horario;
	}
	
	public Disciplina() {
		super();
		this.id = 0;
		this.nome = "";
		this.professor = "";
		this.horario = "";
	}

	@Override
	public String toString() {
		return "Disciplina [id=" + id + ", nome=" + nome + ", professor=" + professor + ", horario=" + horario + ", id=" + id + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((horario == null) ? 0 : horario.hashCode());
		result = prime * result + id;
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result + ((professor == null) ? 0 : professor.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Disciplina other = (Disciplina) obj;
		if (horario == null) {
			if (other.horario != null)
				return false;
		} else if (!horario.equals(other.horario))
			return false;
		if (id != other.id)
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (professor == null) {
			if (other.professor != null)
				return false;
		} else if (!professor.equals(other.professor))
			return false;
		return true;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getProfessor() {
		return professor;
	}

	public void setProfessor(String professor) {
		this.professor = professor;
	}

	public String getHorario() {
		return horario;
	}

	public void setHorario(String horario) {
		this.horario = horario;
	}

	@Override
	public Disciplina clone() {
		// TODO Auto-generated method stub
		return new Disciplina(id, nome, professor, horario);
	}
	
	
	
}
