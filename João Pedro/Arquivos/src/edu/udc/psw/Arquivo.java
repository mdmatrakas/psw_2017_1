package edu.udc.psw;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;

public class Arquivo extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField textField;
	private JTextPane textPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Arquivo frame = new Arquivo();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Arquivo() {
		setTitle("Tratamento de Arquivos");

		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		JMenu mnArquivo = new JMenu("Arquivo");
		menuBar.add(mnArquivo);

		JMenuItem mntmLerTexto = new JMenuItem("Ler texto");
		mnArquivo.add(mntmLerTexto);
		mntmLerTexto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				File arquivo = escolherArquivo();

				if (arquivo != null) {
					lerArquivo(arquivo);
				}

			}

		});

		JMenuItem mntmSalvarTexto = new JMenuItem("Salvar texto");
		mnArquivo.add(mntmSalvarTexto);
		mntmSalvarTexto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				File arquivo = escolherArquivo();

				if (arquivo != null) {
					salvarArquivo(arquivo);
				}

			}

		});
		
		JMenu mnProcessar = new JMenu("Processar");
		menuBar.add(mnProcessar);
		
		JMenuItem mntmDisciplinas = new JMenuItem("Disciplinas");
		mnProcessar.add(mntmDisciplinas);
		mntmDisciplinas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				File arquivo = escolherArquivo();

				if (arquivo != null) {
					processarDisciplina(arquivo);
				}
				
			}
		});

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		textField = new JTextField();
		contentPane.add(textField, BorderLayout.CENTER);
		textField.setColumns(10);

		JScrollPane scrollPane = new JScrollPane();
		contentPane.add(scrollPane, BorderLayout.CENTER);

		textPane = new JTextPane();
		scrollPane.setViewportView(textPane);

	}
	
	private File escolherArquivo() {
		
		JFileChooser fc = new JFileChooser();
		
		fc.setCurrentDirectory(new File(System.getProperty("user.home")));

		FileNameExtensionFilter texFilter = new FileNameExtensionFilter("Tex File", "tex");
		fc.setFileFilter(texFilter);

		FileNameExtensionFilter textFilter = new FileNameExtensionFilter("Text File", "txt");
		fc.addChoosableFileFilter(textFilter);

		int result = fc.showOpenDialog(null);
		if (result == JFileChooser.APPROVE_OPTION) {
			return fc.getSelectedFile();
		}
		
		return null;
		
	}

	private void lerArquivo(File arquivo) {

		StringBuffer buffer = new StringBuffer();

		try {
			Scanner sc = new Scanner(arquivo);
			while (sc.hasNext()) {
				buffer.append(sc.nextLine());
				buffer.append("\n");
			}
			sc.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		textPane.setText(buffer.toString());
		
	}

	private void salvarArquivo(File arquivo) {

		try {
			FileWriter fw = new FileWriter(arquivo);
			fw.append(textPane.getText());
			fw.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	private void processarDisciplina(File arquivo) {
		
		StringBuffer buffer = new StringBuffer();
		
		String linha;
		String campos[];

		try {
			Scanner sc = new Scanner(arquivo);
			while (sc.hasNext()) {
				linha = sc.nextLine();
				campos = linha.split(";");
				
				buffer.append("Disciplina: ");
				buffer.append(campos[0]);
				buffer.append("\n");
				
				buffer.append("Professor: ");
				buffer.append(campos[1]);
				buffer.append("\n");
				
				buffer.append("Hor�rio: ");
				buffer.append(campos[2]);
				buffer.append("\n");
				buffer.append("\n");
			}
			sc.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		textPane.setText(buffer.toString());

	}
	
}
