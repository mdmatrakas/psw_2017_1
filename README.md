# Faculdades Anglo-Americano de Foz do Iguaçu #
# Ciência da Computação #

## Projeto de Software II - 2017 ##

Exemplos de sala de aula para a disciplina de Projeto de Software II - Faculdade Anglo-Americano de Foz do Iguaçu, Professor Miguel Matrakas.

Grande parte dos exemplos são baseados nos códigos do livro Java como Programar, Deitel, Ed Pearson.

### Objetivo do repositório ###

* Exemplos de sala de aula para a disciplina de Projeto de Software II
* Acompanhamento do desenvolvimento de trabalhos acadêmicos
* Entrega de trabalhos práticos

### Como configurar e utilizar ###

Para o desenvolvimento dos exemplos e trabalhos está sendo utilizada a IDE Eclipse Luna.

* Instale os plug-ins de conexão com repositórios GIT no Eclipse ou sua IDE de preferencia.
* Configure o repositório remoto para apontar para este projeto no Bitbucket (https://bitbucket.org/mdmatrakas/psw_2017_1)
* Utilize codificação UTF-16 para salvar os arquivos de código, evitando assim diferenças de codificação em caracteres acentuados entre os IDEs dos participantes do projeto.
* Ao realizar um commit no repositório, forneça uma descrição da funcionalidade implementada, informando quais as classes alteradas.
* Resolva pequenos problemas de cada vez (em cada commit), e procure não realizar commit de códigos com erros, apenas das funcionalidades prontas.
* Monte uma lista de atividades (cronograma), com todas as funcionalidades e alterações que devem ser realizadas por você no projeto.
* Utilize um sistema de gerencia de atividade, ou coloque comentários no código, como o exemplo a seguir, de modo que a IDE mostre a lista de atividades em uma janela apropriada.
```
#!java
//TODO: lembrete da ação a ser realizada, ou alteração necessária no código.
```

* Para cada classe do projeto, utilize o seguinte cabeçalho, assim todos podem entender facilmente o código.
```
#!java
/**
 * Uma frase curta descrevendo a classe.
 * <p>
 * Um texto com maiores explicações a respeito da classe.....
 * <p>
 * A podem ser utilizadas tags HTML para formatar o conteúdo da descrição.
 * <p>
 * Acrescentar uma seção para documentar as alterações realizadas no código da classe, informando a data, o autor e uma pequena descrição das alterações realizadas. Este tipo de conteúdo normalmente não faz parte da documentação, mas para acompanhamento da evolução do trabalho e razões didáticas, o padrão apresentado a seguir deverá ser adotado para todas as classes do trabalho.
 * <p>
 * <h3>Modificações:</h3>
 * <ol>
 *     <li><b>MDMatrakas</b> - 2017/03/07</li>
 *     <ul>
 *         <li>Alterações no padrão JavaDoc a ser utilizado para documentar as classes do projeto.
 *     </ul>
 * </ol>
 * 
 * @author Fulano de Tal
 * @version 0.0.1
 * @since 0.0.1
 */
```

* Ao realizar um commit, no comentário especificar qual a atividade/requisito do projeto está sendo implementado no código (de acordo com a lista abaixo).
* Realizar pelo menos um commit para cada atividade/requisito.

### Responsável ###

* Miguel Matrakas

______

### Requisitos para o Projeto - Trabalho da disciplina ###

* Escolhe disciplinas, verifica se não há conflitos e monta um horário de aula
* Telas de cadastro de disciplinas, horários, professores (CRUD)
* Geração arquivo texto e misto
* Ler arquivos para mostrar informações
* Modo de texto e binário
* Utilização de SGBD 
* Telas de login/senha e upload de imagem do usuário*