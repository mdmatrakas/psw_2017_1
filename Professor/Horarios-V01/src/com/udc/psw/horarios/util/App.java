/*
 * Descri��o do projeto em desenvolvimento nas aulas de Projeto de Software II - 2017-1
 * -------------
 * Conve��es:
 * - Um porjeto por aluno
 * - Nome do aluno no projeto para identifica��o
 * - Todos os projeto no mesmo reposit�rio - Bitbucket
 * - Todas as classes e m�todos dever�o estar documentoados - JavaDoc
 * 
 * ------------------------
 * 09/02/2017
 *
 * Descri��o dos dados:
 *
 *	-> Disciplina
 *		- Nome
 *		- Professor
 *		- Hor�rio
 *	
 *	-> Professor
 *		- Nome
 *
 *	-> Hor�rio
 *		- Dias (enum)
 *		- 1o / 2o (enum)
 * 
 * ------------------------
 * 15/02/2017
 * 
 * Utiliza��o das classes Swing para criar uma janela com menu dispondo de comandos para
 * ler e salvar arquivos de texto, com a sele��o do arquivo por caixa de dialogo pr�pria.
 * 
 * Dispon�vel tamb�m comando para ler um arquivo com registros de valores separados por ";",
 * separando cada um dos registros e seus respectivo campos.
 * 
 * Como atividade:
 *   -> utilizar estas t�cnicas para criar e processar os dados definidos para o app de hor�rios.
 */
package com.udc.psw.horarios.util;

/**
 * @author MDMatrakas
 *
 */
public class App {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
