import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileNameExtensionFilter;

import model.Disciplina;

import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;

public class FrameArquivo extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextPane textPane;
	private JMenuBar menuBar;
	private JMenu mnArquivo;
	private JMenuItem mntmLerTexto;
	private JMenuItem mntmSalvarTexto;
	private JMenu mnProcessar;
	private JMenuItem mntmDisciplina;
	private JTextPane textFileInfo;
	private JTextPane txtLines;
	private JMenuItem mntmLerSerial;
	private JMenuItem mntmSalvarSerial;
	
	private List<Disciplina> l;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrameArquivo frame = new FrameArquivo();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrameArquivo() {
		l = new ArrayList<Disciplina>();
		setTitle("Tratamento de Arquivos");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		
		menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		mnArquivo = new JMenu("Arquivo");
		menuBar.add(mnArquivo);
		
		mntmLerTexto = new JMenuItem("Ler texto");
		mntmLerTexto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				File arquivo = escolherArquivo();
				if(arquivo != null) {					
					lerArquivo(arquivo);
				}
			}
		});
		mnArquivo.add(mntmLerTexto);
		
		mntmSalvarTexto = new JMenuItem("Salvar texto");
		mntmSalvarTexto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {				
				File arquivo = escolherArquivo();
				if(arquivo != null) {					
					salvarArquivo(arquivo);
				}
			}
		});
		mnArquivo.add(mntmSalvarTexto);
		
		mntmLerSerial = new JMenuItem("Ler serial");
		mntmLerSerial.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				File arquivo = escolherArquivo();
				if(arquivo != null) {					
					lerSerial(arquivo);
				}
			}
		});
		mnArquivo.add(mntmLerSerial);
		
		mntmSalvarSerial = new JMenuItem("Salvar serial");
		mntmSalvarSerial.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				File arquivo = escolherArquivo();
				if(arquivo != null) {					
					salvarSerial(arquivo);
				}
			}
		});
		mnArquivo.add(mntmSalvarSerial);
		
		mnProcessar = new JMenu("Processar");
		menuBar.add(mnProcessar);
		
		mntmDisciplina = new JMenuItem("Disciplina");
		mntmDisciplina.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				File arquivo = escolherArquivo();
				if(arquivo != null) {					
					processarDisciplina(arquivo);
				}
			}
		});
		mnProcessar.add(mntmDisciplina);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JScrollPane scrollPane = new JScrollPane();
		contentPane.add(scrollPane, BorderLayout.CENTER);
		
		textPane = new JTextPane();
		scrollPane.setViewportView(textPane);
		
		textFileInfo = new JTextPane();
		textFileInfo.setEnabled(false);
		textFileInfo.setEditable(false);
		textFileInfo.setBackground(Color.LIGHT_GRAY);
		textFileInfo.setForeground(new Color(165, 42, 42));
		textFileInfo.setText("File info");
		scrollPane.setColumnHeaderView(textFileInfo);
		
		txtLines = new JTextPane();
		txtLines.setEnabled(false);
		txtLines.setEditable(false);
		txtLines.setForeground(new Color(0, 128, 128));
		txtLines.setBackground(Color.LIGHT_GRAY);
		scrollPane.setRowHeaderView(txtLines);
		
	}
	
	protected void salvarSerial(File arquivo) {
		try {
			ObjectOutputStream out = new ObjectOutputStream(
					new FileOutputStream(arquivo));
			for(Disciplina d : l) {
				out.writeObject(d);
			}
			out.close();
			
		} catch(IOException e) {
			e.printStackTrace();
		}
	}

	protected void lerSerial(File arquivo) {
		StringBuffer buffer = new StringBuffer();
		StringBuffer bufferLines = new StringBuffer();
		int lines = 0;
		
		
		if(arquivo.exists()) {
			String info = String.format("%s%s\n%s%s\n%s%s",
					"Data da ultima modificação: ",
					new SimpleDateFormat("dd/MM/yyyy - HH:mm:ss").format(arquivo.lastModified()),
					"Tamanho: ", arquivo.length(),
					"Caminho: ", arquivo.getAbsolutePath());
			
			textFileInfo.setText(info);
		}
		
		try 
		{
			ObjectInputStream in = new ObjectInputStream(new FileInputStream(arquivo));
			Disciplina d = null;
			while((d = (Disciplina)in.readObject()) != null ) {
				l.add(d);
			}
			in.close();
		} catch(IOException e) {
			e.printStackTrace();
			
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		for(Disciplina d : l) {
			buffer.append(d.toString() + "\n");
			bufferLines.append(++lines);
			bufferLines.append("\n");
		}
		
		textPane.setText(buffer.toString());
		txtLines.setText(bufferLines.toString());
	}

	private void processarDisciplina(File arquivo) {
		StringBuffer buffer = new StringBuffer();
		
		String linha;
		String campos[];
		
		
		try {
			Scanner sc = new Scanner(arquivo);
			while(sc.hasNext()) {
				linha = sc.nextLine();
				campos = linha.split(";");
				
				l.clear();
				
				Disciplina d = new Disciplina(Integer.parseInt(campos[0]), campos[1], campos[2], campos[3]);
				l.add(d);
				
//				buffer.append("Disciplina: ");
//				buffer.append(campos[0]);
//				buffer.append("\n");
//				
//				buffer.append("Professor: ");
//				buffer.append(campos[1]);
//				buffer.append("\n");
//				
//				buffer.append("Horário: ");
//				buffer.append(campos[2]);
//				buffer.append("\n");
			}
			sc.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		for(Disciplina d : l) {
			buffer.append(d.toString() + "\n");
		}
		
		textPane.setText(buffer.toString());		
	}

	private File escolherArquivo() {
		JFileChooser fc = new JFileChooser();
		fc.setCurrentDirectory(new File(System.getProperty("user.home")));
		FileNameExtensionFilter textFilter = new FileNameExtensionFilter("Text file", "txt");
		fc.addChoosableFileFilter(textFilter);
		
		int result = fc.showOpenDialog(null);
		if(result == JFileChooser.APPROVE_OPTION) {
			 return fc.getSelectedFile();						
		}
		return null;
	}
	
	private void lerArquivo(File arquivo) {
		StringBuffer buffer = new StringBuffer();
		StringBuffer bufferLines = new StringBuffer();
		int lines = 0;
		
		
		if(arquivo.exists()) {
			String info = String.format("%s%s\n%s%s\n%s%s",
					"Data da ultima modificação: ",
					new SimpleDateFormat("dd/MM/yyyy - HH:mm:ss").format(arquivo.lastModified()),
					"Tamanho: ", arquivo.length(),
					"Caminho: ", arquivo.getAbsolutePath());
			
			textFileInfo.setText(info);
		}
		
		try {
			Scanner sc = new Scanner(arquivo);
			while(sc.hasNext()) {
				buffer.append(sc.nextLine());
				buffer.append("\n");
				
				bufferLines.append(++lines);
				bufferLines.append("\n");
			}
			sc.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		textPane.setText(buffer.toString());
		txtLines.setText(bufferLines.toString());
		
		
	}
	
	private void salvarArquivo(File arquivo) {
		try {
			FileWriter fw = new FileWriter(arquivo);
			fw.append(textPane.getText());
			fw.close();
		} catch(IOException e) {
			e.printStackTrace();
		}
	}

}
