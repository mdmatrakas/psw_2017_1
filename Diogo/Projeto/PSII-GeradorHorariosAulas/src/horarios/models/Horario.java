package horarios.models;

public class Horario {
	String hora;
	String minuto;
	Disciplina disciplina;
	
	public Horario(String hora, String minuto, Disciplina disciplina) {
		this.hora = hora;
		this.minuto = minuto;
		this.disciplina = disciplina;
	}

	public String getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	public String getMinuto() {
		return minuto;
	}

	public void setMinuto(String minuto) {
		this.minuto = minuto;
	}

	public Disciplina getDisciplina() {
		return disciplina;
	}

	public void setDisciplina(Disciplina disciplina) {
		this.disciplina = disciplina;
	}
	
	
}
