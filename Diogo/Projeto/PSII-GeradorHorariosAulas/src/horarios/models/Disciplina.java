package horarios.models;

public class Disciplina {
	String nome;
	Professor professor;
	int cargaHoraria;
	
	public Disciplina(String nome, Professor professor, int cargaHoraria) {
		this.nome = nome;
		this.professor = professor;
		this.cargaHoraria = cargaHoraria;
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Professor getProfessor() {
		return professor;
	}
	public void setProfessor(Professor professor) {
		this.professor = professor;
	}
	public int getCargaHoraria() {
		return cargaHoraria;
	}
	public void setCargaHoraria(int cargaHoraria) {
		this.cargaHoraria = cargaHoraria;
	}
	
	
}
