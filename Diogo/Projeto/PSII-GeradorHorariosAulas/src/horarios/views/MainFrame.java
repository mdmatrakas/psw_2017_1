package horarios.views;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import java.awt.event.ActionListener;
import java.io.File;
import java.awt.event.ActionEvent;

public class MainFrame extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private final String documentsPath = System.getProperty("user.home") + "/Documents";

	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainFrame frame = new MainFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MainFrame() {
		setTitle("Gerenciador de Horários");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnArquivo = new JMenu("Arquivo");
		menuBar.add(mnArquivo);
		
		JMenuItem mntmNovoHorrio = new JMenuItem("Novo Horário");
		mnArquivo.add(mntmNovoHorrio);
		
		JMenuItem mntmSalvar = new JMenuItem("Salvar Horário");
		mntmSalvar.setEnabled(false);
		mnArquivo.add(mntmSalvar);
		
		JMenuItem mntmExportar = new JMenuItem("Exportar...");
		mntmExportar.setEnabled(false);
		mnArquivo.add(mntmExportar);
		
		JMenuItem mntmSuasInformaes = new JMenuItem("Suas Informações");
		mnArquivo.add(mntmSuasInformaes);
		
		JMenuItem mntmTrocarUsurio = new JMenuItem("Trocar Usuário");
		mnArquivo.add(mntmTrocarUsurio);
		
		JMenuItem mntmSair = new JMenuItem("Sair");
		mnArquivo.add(mntmSair);
		
		JMenu mnInserir = new JMenu("Inserir");
		menuBar.add(mnInserir);
		
		JMenu mnProfessor = new JMenu("Professor");
		mnInserir.add(mnProfessor);
		
		JMenuItem mntmNovoProfessor = new JMenuItem("Novo Professor");
		mnProfessor.add(mntmNovoProfessor);
		
		JMenuItem mntmDoArquivo = new JMenuItem("Do Arquivo...");
		mntmDoArquivo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser chooser = new JFileChooser();
				chooser.setFileFilter(new FileNameExtensionFilter("Arquivos de Texto", "txt"));
				chooser.setAcceptAllFileFilterUsed(false);
				chooser.setDialogTitle("Selecione o arquivo de Professores para carregamento");
				chooser.setCurrentDirectory(new File(documentsPath));
				int result = chooser.showDialog(null, "Selecionar");
				
			}
		});
		mnProfessor.add(mntmDoArquivo);
		
		JMenu mnDisciplina = new JMenu("Disciplina");
		mnInserir.add(mnDisciplina);
		
		JMenuItem mntmNovaDisciplina = new JMenuItem("Nova Disciplina");
		mnDisciplina.add(mntmNovaDisciplina);
		
		JMenuItem mntmDoArquivo_1 = new JMenuItem("Do Arquivo...");
		mntmDoArquivo_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser chooser = new JFileChooser();
				chooser.setFileFilter(new FileNameExtensionFilter("Arquivos de Texto", "txt"));
				chooser.setAcceptAllFileFilterUsed(false);
				chooser.setDialogTitle("Selecione o arquivo de Disciplinas para carregamento");
				chooser.setCurrentDirectory(new File(documentsPath));
				int result = chooser.showDialog(null, "Selecionar");
			}
		});
		mnDisciplina.add(mntmDoArquivo_1);
		
		JMenu mnUsurio = new JMenu("Usuário");
		mnInserir.add(mnUsurio);
		
		JMenuItem mntmAdicionarUsurio = new JMenuItem("Adicionar Usuário");
		mnUsurio.add(mntmAdicionarUsurio);
		
		JMenuItem mntmDoArquivo_2 = new JMenuItem("Do Arquivo...");
		mntmDoArquivo_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser chooser = new JFileChooser();
				chooser.setFileFilter(new FileNameExtensionFilter("Arquivos de Texto", "txt"));
				chooser.setAcceptAllFileFilterUsed(false);
				chooser.setDialogTitle("Selecione o arquivo de Usuários para carregamento");
				chooser.setCurrentDirectory(new File(documentsPath));
				int result = chooser.showDialog(null, "Selecionar");
			}
		});
		mnUsurio.add(mntmDoArquivo_2);
		
		JMenu mnAjuda = new JMenu("Ajuda");
		menuBar.add(mnAjuda);
		
		JMenuItem mntmSobre = new JMenuItem("Sobre");
		mnAjuda.add(mntmSobre);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
	}

}
