package Interface;

import javax.swing.JFrame;
import java.awt.Color;
import javax.swing.JTextField;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import javax.swing.JButton;
import java.awt.Insets;
import javax.swing.SwingConstants;

public class cadastroDeProfessor extends JFrame {
	private JTextField txtProfessor;
	

	public cadastroDeProfessor() {
		setTitle("Cadastro de Professor");
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 0, 0, 0 };
		gridBagLayout.rowHeights = new int[] { 0, 0, 0, 0, 0 };
		gridBagLayout.columnWeights = new double[] { 0.0, 1.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		getContentPane().setLayout(gridBagLayout);

		txtProfessor = new JTextField();
		txtProfessor.setToolTipText("Insira o Nome do Professor Novo.");
		txtProfessor.setFont(txtProfessor.getFont().deriveFont(13f));
		txtProfessor.setForeground(Color.LIGHT_GRAY);
		txtProfessor.setText("Professor ");
		GridBagConstraints gbc_txtProfessor = new GridBagConstraints();
		gbc_txtProfessor.insets = new Insets(0, 0, 5, 0);
		gbc_txtProfessor.gridx = 1;
		gbc_txtProfessor.gridy = 1;
		getContentPane().add(txtProfessor, gbc_txtProfessor);
		txtProfessor.setColumns(10);

		JButton btnOk = new JButton("OK");
		GridBagConstraints gbc_btnOk = new GridBagConstraints();
		gbc_btnOk.gridx = 1;
		gbc_btnOk.gridy = 3;
		getContentPane().add(btnOk, gbc_btnOk);
	}

}
