package Interface;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "DISCIPLINAS")
@XmlAccessorType(XmlAccessType.FIELD)

public class Disciplinas {
	@XmlElement(name = "DISCIPLINA")

	List<Disciplina> lista;

	public Disciplinas() {
		super();
		lista = null;
	}

	public List<Disciplina> getLista() {
		return lista;
	}

	public void setLista(List<Disciplina> lista) {
		this.lista = lista;
	}
}
