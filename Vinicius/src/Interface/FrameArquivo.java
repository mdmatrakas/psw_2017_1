package Interface;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import java.awt.Color;

public class FrameArquivo extends JFrame {

	private JPanel contentPane;
	private JTextPane textPane;
	private JMenuBar menuBar;
	private JMenu mnArquivo;
	private JMenuItem mntmLerTexto;
	private JMenuItem mntmSalvarTexto;
	private JMenu mnProcessar;
	private JMenuItem mntmDisciplina;
	private JTextPane textPane_1;
	private JTextPane numLinhas;
	private JMenuItem mntmLerSerial;
	private JMenuItem mntmSalvarSerial;
	private List<Disciplina> lista;
	private JMenuItem mntmGerarXml;
	private JMenuItem mntmLerXml;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrameArquivo frame = new FrameArquivo();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrameArquivo() {
		lista = new ArrayList<>();
		setTitle("Tratamento de Arquivos");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);

		menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		mnArquivo = new JMenu("Arquivo");
		menuBar.add(mnArquivo);

		mntmSalvarTexto = new JMenuItem("Salvar texto");
		mntmSalvarTexto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				File arquivo = escolherArquivo();
				if (arquivo != null) {
					salvarArquivo(arquivo);
				}
			}
		});
		mnArquivo.add(mntmSalvarTexto);

		mntmLerTexto = new JMenuItem("Ler texto");
		mntmLerTexto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				File arquivo = escolherArquivo();
				if (arquivo != null) {
					lerArquivo(arquivo);
				}
			}
		});
		mnArquivo.add(mntmLerTexto);

		mntmSalvarSerial = new JMenuItem("Salvar Serial");
		mnArquivo.add(mntmSalvarSerial);
		mntmSalvarSerial.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arq0) {
				File arquivo = escolherArquivo();
				if (arquivo != null) {
					salvarSerial(arquivo);
				}

			}
		});

		mntmLerSerial = new JMenuItem("Ler Serial");
		mnArquivo.add(mntmLerSerial);
		mntmLerSerial.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arq0) {
				File arquivo = escolherArquivo();
				if (arquivo != null) {
					lerSerial(arquivo);
				}

			}
		});

		mnProcessar = new JMenu("Processar");
		menuBar.add(mnProcessar);

		mntmDisciplina = new JMenuItem("Disciplina");
		mntmDisciplina.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				File arquivo = escolherArquivo();
				if (arquivo != null) {
					processarDisciplina(arquivo);
				}
			}
		});
		mnProcessar.add(mntmDisciplina);

		mntmGerarXml = new JMenuItem("Gerar Xml");
		mntmGerarXml.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				Disciplina disciplina = new Disciplina(35, "programacao", "asdasdasd", "teste");
				Disciplina disciplinaq = new Disciplina(35, "progxxxxramacao", "asdasxxdasd", "tesxte");
				
				List<Disciplina> l = new ArrayList<Disciplina>();
				l.add(disciplina);
				l.add(disciplinaq);
				Disciplinas disciplinas = new Disciplinas();
				disciplinas.setLista(l);
				File arquivo = escolherArquivo();

				JAXBContext context;
				try {
					context = JAXBContext.newInstance(Disciplinas.class);
					Marshaller m = context.createMarshaller();
					m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

					m.marshal(disciplinas, arquivo);
				} catch (JAXBException e) {
					e.printStackTrace();
				}

			}

		});
		mnProcessar.add(mntmGerarXml);

		mntmLerXml = new JMenuItem("Ler Xml");
		mntmLerXml.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Disciplinas disciplinas = null;
				File arquivo = escolherArquivo();

				JAXBContext contex;
				try {
					contex = JAXBContext.newInstance(Disciplinas.class);
					Unmarshaller un = contex.createUnmarshaller();
					disciplinas = (Disciplinas) un.unmarshal(arquivo);
				} catch (JAXBException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				List<Disciplina> l = disciplinas.getLista();
				StringBuffer buffer = new StringBuffer();
				StringBuffer bufferLinha = new StringBuffer();
				int linha =0;
				for (Disciplina d:l){
					buffer.append(d.toString()+"\n");
					bufferLinha.append(++linha + "\n");
				}
				
				textPane.setText(buffer.toString());
				textPane_1.setText(bufferLinha.toString());
			}
		});
		mnProcessar.add(mntmLerXml);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		JScrollPane scrollPane = new JScrollPane();
		contentPane.add(scrollPane, BorderLayout.CENTER);

		textPane = new JTextPane();
		scrollPane.setViewportView(textPane);

		textPane_1 = new JTextPane();
		textPane_1.setEditable(false);
		textPane_1.setForeground(Color.RED);
		textPane_1.setBackground(Color.LIGHT_GRAY);
		scrollPane.setColumnHeaderView(textPane_1);

		numLinhas = new JTextPane();
		numLinhas.setEditable(false);
		scrollPane.setRowHeaderView(numLinhas);

	}

	private void processarDisciplina(File arquivo) {

		StringBuffer buffer = new StringBuffer();

		String linha;
		String campos[];
		lista.clear();
		try {
			Scanner sc = new Scanner(arquivo);
			while (sc.hasNext()) {
				linha = sc.nextLine();
				campos = linha.split(";");
				Disciplina d = new Disciplina(Integer.parseInt(campos[0]), campos[1], campos[2], campos[3]);
				lista.add(d);

			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for (Disciplina d : lista)
			buffer.append(d.toString() + "\n");
		textPane.setText(buffer.toString());
	}

	private File escolherArquivo() {
		JFileChooser fc = new JFileChooser();
		fc.setCurrentDirectory(new File(System.getProperty("user.home")));
		FileNameExtensionFilter textFilter = new FileNameExtensionFilter("Text file", "txt");
		FileNameExtensionFilter xmlFilter = new FileNameExtensionFilter("xml file", "xml");
		fc.addChoosableFileFilter(textFilter);
		fc.addChoosableFileFilter(xmlFilter);
		int result = fc.showOpenDialog(null);
		if (result == JFileChooser.APPROVE_OPTION) {
			return fc.getSelectedFile();
		}
		return null;
	}

	private void lerArquivo(File arquivo) {
		StringBuffer buffer = new StringBuffer();
		StringBuffer bufferLines = new StringBuffer();
		int lines = 0;

		if (arquivo.exists()) {
			String info = String.format("%s%s\n%s%s\n%s%s", "Data da ultima modificacoes: ",
					new SimpleDateFormat("dd/MM/yyyy - HH:mm:ss").format(arquivo.lastModified()), "Tamanho: ",
					arquivo.length(), "Caminho: ", arquivo.getAbsolutePath());

			textPane_1.setText(info);
		}

		try {
			Scanner sc = new Scanner(arquivo);
			while (sc.hasNext()) {
				buffer.append(sc.nextLine());
				buffer.append("\n");

				bufferLines.append(++lines);
				bufferLines.append("\n");
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		textPane.setText(buffer.toString());
		numLinhas.setText(bufferLines.toString());

	}

	private void lerSerial(File arquivo) {
		StringBuffer buffer = new StringBuffer();
		StringBuffer bufferLines = new StringBuffer();
		int lines = 0;

		if (arquivo.exists()) {
			String info = String.format("%s%s\n%s%s\n%s%s", "Data da ultima modificacoes: ",
					new SimpleDateFormat("dd/MM/yyyy - HH:mm:ss").format(arquivo.lastModified()), "Tamanho: ",
					arquivo.length(), "Caminho: ", arquivo.getAbsolutePath());

			textPane_1.setText(info);
		}

		ObjectInputStream in = null;
		try {
			in = new ObjectInputStream(new FileInputStream(arquivo));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		while (true) {// terminar depois os catch
			try {
				Disciplina d = (Disciplina) in.readObject();
			} catch (ClassNotFoundException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	private void salvarArquivo(File arquivo) {
		try {
			FileWriter fw = new FileWriter(arquivo);
			fw.append(textPane.getText());
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void salvarSerial(File arquivo) {
		try {
			ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(arquivo));
			for (Disciplina d : lista)
				out.writeObject(d);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
