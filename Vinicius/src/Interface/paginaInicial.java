package Interface;

import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JMenu;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.border.LineBorder;
import java.awt.Color;
import javax.swing.JPopupMenu;
import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.nio.file.FileAlreadyExistsException;
import java.util.Scanner;
import javax.swing.JPanel;
import java.awt.GridLayout;
import javax.swing.JTextField;
import javax.swing.JButton;

public class paginaInicial extends JFrame {
	private JTextField textField;
	private JTextField textoIdentificador;
	
	public paginaInicial() {
		getContentPane().setLayout(null);
		
		textField = new JTextField();
		textField.setBounds(134, 93, 173, 68);
		getContentPane().add(textField);
		textField.setColumns(10);
		
		JButton proximo = new JButton(">");
		proximo.setBounds(266, 172, 41, 23);
		getContentPane().add(proximo);
		
		JButton btnNewButton = new JButton("<");
		btnNewButton.setBounds(134, 172, 41, 23);
		getContentPane().add(btnNewButton);
		
		textoIdentificador = new JTextField();
		textoIdentificador.setEditable(false);
		textoIdentificador.setEnabled(false);
		textoIdentificador.setBounds(134, 62, 173, 20);
		getContentPane().add(textoIdentificador);
		textoIdentificador.setColumns(10);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnAdicionar = new JMenu("Adicionar");
		menuBar.add(mnAdicionar);
		
		JMenuItem mntmNovoCurso = new JMenuItem("Novo Curso");
		mnAdicionar.add(mntmNovoCurso);
		
		JMenuItem mntmNovoprofessor = new JMenuItem("NovoProfessor");
		mnAdicionar.add(mntmNovoprofessor);
		
		JMenu mnAluno = new JMenu("Aluno");
		menuBar.add(mnAluno);
		
		JMenuItem mntmCriarGradeEscolar = new JMenuItem("Criar Grade ");
		mnAluno.add(mntmCriarGradeEscolar);
		
		JMenuItem menuItem = new JMenuItem("");
		menuBar.add(menuItem);
	}
	private static void addPopup(Component component, final JPopupMenu popup) {
		component.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}
			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}
			private void showMenu(MouseEvent e) {
				popup.show(e.getComponent(), e.getX(), e.getY());
			}
		});
	}
	// mudar para um classe unica 
	private void lerArquivo(){
		File arquivo = new File("E:\\Documentos\b");
		StringBuffer buffer = new StringBuffer();
	
			Scanner sc;
			try {
				sc = new Scanner(arquivo);
				while(sc.hasNext()){
					buffer.append(sc.nextLine());
					buffer.append("\n");
				}
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		
	}
}
