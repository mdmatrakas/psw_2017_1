package Interface;

import javax.swing.JFrame;
import net.miginfocom.swing.MigLayout;
import javax.swing.JTextField;
import javax.swing.JList;
import javax.swing.JInternalFrame;
import java.awt.BorderLayout;
import javax.swing.JSpinner;
import javax.swing.JScrollBar;
import java.awt.FlowLayout;
import javax.swing.AbstractListModel;
import javax.swing.ListSelectionModel;
import javax.swing.JPanel;
import java.awt.CardLayout;
import javax.swing.JButton;
import java.awt.Color;
import javax.swing.JComboBox;

public class novaMateria extends JFrame{
	private JTextField txtNovaMateria;
	public novaMateria() {
		setTitle("Cadastro Nova Materia");
		getContentPane().setLayout(null);
		
		JButton btnNewButton = new JButton("New button");
		btnNewButton.setBounds(155, 227, 124, 23);
		getContentPane().add(btnNewButton);
		
		txtNovaMateria = new JTextField();
		txtNovaMateria.setForeground(Color.LIGHT_GRAY);
		txtNovaMateria.setText("Nova Materia");
		txtNovaMateria.setToolTipText("Nova Materia");
		txtNovaMateria.setBounds(72, 35, 243, 23);
		getContentPane().add(txtNovaMateria);
		txtNovaMateria.setColumns(10);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setBounds(72, 116, 207, 23);
		getContentPane().add(comboBox);
	}
}
