package br.edu.udc.psw2.dominio.horario;

import javax.management.RuntimeErrorException;

public class Horas {

	private int horas;
	private int minutos;

	public Horas(int horas, int minutos) {
		this.horas = horas;
		this.minutos = minutos;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + horas;
		result = prime * result + minutos;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Horas other = (Horas) obj;
		if (horas != other.horas)
			return false;
		if (minutos != other.minutos)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return this.horas + ":" + this.minutos;
	}

	public int getHoras() {
		return horas;
	}

	public void setHoras(int horas) {
		if (horas < 0 || horas > 23)
			throw new RuntimeErrorException(null, "Quantidade de horas inv�lida");

		this.horas = horas;
	}

	public int getMinutos() {
		return minutos;
	}

	public void setMinutos(int minutos) {
		if (minutos < 0 || minutos > 59)
			throw new RuntimeErrorException(null, "Quantidade de minutos inv�lida");

		this.minutos = minutos;
	}
}
