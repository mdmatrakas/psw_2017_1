package br.edu.udc.psw2.janela.professor;

import javax.swing.JFrame;
import java.awt.BorderLayout;
import javax.swing.JPanel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.JLabel;

public class CaixaProfessor extends JFrame {
	private static final long serialVersionUID = 8393049096211567093L;
	private JTextField nomeProfessor;
	public CaixaProfessor() {
		setTitle("Professor");
		getContentPane().setLayout(new BorderLayout(0, 0));
		
		JPanel panel_escolha = new JPanel();
		getContentPane().add(panel_escolha, BorderLayout.SOUTH);
		
		JButton botaoCancelar = new JButton("Cancelar");
		botaoCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		panel_escolha.add(botaoCancelar);
		
		JButton botaoOk = new JButton("OK");
		botaoCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		panel_escolha.add(botaoOk);
		
		JPanel painel_cadastro = new JPanel();
		getContentPane().add(painel_cadastro, BorderLayout.CENTER);
		
		JLabel lblNome = new JLabel("Nome:");
		painel_cadastro.add(lblNome);
		
		nomeProfessor = new JTextField();
		painel_cadastro.add(nomeProfessor);
		nomeProfessor.setColumns(10);
	}
}
