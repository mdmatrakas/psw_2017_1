package br.edu.udc.psw2.janela.disciplina;

import javax.swing.JFrame;
import java.awt.BorderLayout;
import javax.swing.JPanel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.layout.FormSpecs;
import javax.swing.JCheckBox;

public class CaixaDisciplina extends JFrame {
	private static final long serialVersionUID = 8393049096211567093L;
	private JTextField textField;
	private JTextField textField_2;
	private JTextField textField_1;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;
	private JTextField textField_6;
	private JTextField textField_7;
	private JTextField textField_8;
	private JTextField textField_9;
	private JTextField textField_10;
	public CaixaDisciplina() {
		setTitle("Disciplina");
		getContentPane().setLayout(new BorderLayout(0, 0));
		
		JPanel panel_escolha = new JPanel();
		getContentPane().add(panel_escolha, BorderLayout.SOUTH);
		
		JButton botaoCancelar = new JButton("Cancelar");
		botaoCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		panel_escolha.add(botaoCancelar);
		
		JButton botaoOk = new JButton("OK");
		botaoCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		panel_escolha.add(botaoOk);
		
		JPanel panel = new JPanel();
		getContentPane().add(panel, BorderLayout.NORTH);
		panel.setLayout(new FormLayout(new ColumnSpec[] {
				FormSpecs.RELATED_GAP_COLSPEC,
				FormSpecs.DEFAULT_COLSPEC,
				FormSpecs.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("default:grow"),
				FormSpecs.RELATED_GAP_COLSPEC,
				FormSpecs.DEFAULT_COLSPEC,
				FormSpecs.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("default:grow"),
				FormSpecs.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("default:grow"),},
			new RowSpec[] {
				FormSpecs.RELATED_GAP_ROWSPEC,
				FormSpecs.DEFAULT_ROWSPEC,
				FormSpecs.RELATED_GAP_ROWSPEC,
				FormSpecs.DEFAULT_ROWSPEC,
				FormSpecs.RELATED_GAP_ROWSPEC,
				FormSpecs.DEFAULT_ROWSPEC,
				FormSpecs.RELATED_GAP_ROWSPEC,
				FormSpecs.DEFAULT_ROWSPEC,
				FormSpecs.RELATED_GAP_ROWSPEC,
				FormSpecs.DEFAULT_ROWSPEC,
				FormSpecs.RELATED_GAP_ROWSPEC,
				FormSpecs.DEFAULT_ROWSPEC,
				FormSpecs.RELATED_GAP_ROWSPEC,
				FormSpecs.DEFAULT_ROWSPEC,
				FormSpecs.RELATED_GAP_ROWSPEC,
				FormSpecs.DEFAULT_ROWSPEC,}));
		
		JLabel lblMatria = new JLabel("Mat\u00E9ria:");
		panel.add(lblMatria, "2, 2, right, default");
		
		textField = new JTextField();
		panel.add(textField, "4, 2, 5, 1, fill, default");
		textField.setColumns(10);
		
		JLabel lblProfessor = new JLabel("Professor:");
		panel.add(lblProfessor, "2, 4, right, default");
		
		JComboBox comboBox = new JComboBox();
		panel.add(comboBox, "4, 4, 5, 1, fill, default");
		
		JLabel lblHorario = new JLabel("--- Horario ---");
		panel.add(lblHorario, "1, 6, 10, 1, center, default");
		
		JCheckBox chckbxSegunda = new JCheckBox("Segunda");
		panel.add(chckbxSegunda, "2, 8");
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		panel.add(textField_1, "4, 8, fill, default");
		
		JLabel lblAt = new JLabel("At\u00E9");
		panel.add(lblAt, "6, 8, right, default");
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		panel.add(textField_2, "8, 8, fill, default");
		
		JCheckBox chckbxTera = new JCheckBox("Ter\u00E7a");
		panel.add(chckbxTera, "2, 10");
		
		textField_3 = new JTextField();
		textField_3.setColumns(10);
		panel.add(textField_3, "4, 10, fill, default");
		
		JLabel label = new JLabel("At\u00E9");
		panel.add(label, "6, 10, right, default");
		
		textField_7 = new JTextField();
		textField_7.setColumns(10);
		panel.add(textField_7, "8, 10, fill, default");
		
		JCheckBox chckbxQuarta = new JCheckBox("Quarta");
		panel.add(chckbxQuarta, "2, 12");
		
		textField_4 = new JTextField();
		textField_4.setColumns(10);
		panel.add(textField_4, "4, 12, fill, default");
		
		JLabel label_1 = new JLabel("At\u00E9");
		panel.add(label_1, "6, 12, right, default");
		
		textField_8 = new JTextField();
		textField_8.setColumns(10);
		panel.add(textField_8, "8, 12, fill, default");
		
		JCheckBox chckbxQuinta = new JCheckBox("Quinta");
		panel.add(chckbxQuinta, "2, 14");
		
		textField_5 = new JTextField();
		textField_5.setColumns(10);
		panel.add(textField_5, "4, 14, fill, default");
		
		JLabel label_2 = new JLabel("At\u00E9");
		panel.add(label_2, "6, 14, right, default");
		
		textField_9 = new JTextField();
		textField_9.setColumns(10);
		panel.add(textField_9, "8, 14, fill, default");
		
		JCheckBox chckbxSexta = new JCheckBox("Sexta");
		panel.add(chckbxSexta, "2, 16");
		
		textField_6 = new JTextField();
		textField_6.setColumns(10);
		panel.add(textField_6, "4, 16, fill, default");
		
		JLabel label_3 = new JLabel("At\u00E9");
		panel.add(label_3, "6, 16, right, default");
		
		textField_10 = new JTextField();
		textField_10.setColumns(10);
		panel.add(textField_10, "8, 16, fill, default");
	}
}
