package br.edu.udc.psw2.janela.main;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.JMenu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

public class MainMenu extends JFrame {
	private static final long serialVersionUID = 8393049096211567093L;
	public MainMenu() {
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnAdicionar = new JMenu("Adicionar");
		menuBar.add(mnAdicionar);
		
		JMenuItem mntmNovoprofessor = new JMenuItem("Novo Professor");
		mntmNovoprofessor.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				//TODO CONTROLE - Adicionar Professor
			}
		});
		mnAdicionar.add(mntmNovoprofessor);
		
		JMenuItem mntmNovaDisciplina = new JMenuItem("Nova Disciplina");
		mnAdicionar.add(mntmNovaDisciplina);
		
		JMenu mnArquivo = new JMenu("Arquivo");
		menuBar.add(mnArquivo);
		
		JMenu mnArquivoTexto = new JMenu("Arquivo Texto");
		mnArquivo.add(mnArquivoTexto);
		
		JMenuItem mntmImportar = new JMenuItem("Importar");
		mnArquivoTexto.add(mntmImportar);
		
		JMenuItem mntmExportar = new JMenuItem("Exportar");
		mnArquivoTexto.add(mntmExportar);
		
		JMenu mnArquivoBinario = new JMenu("Arquivo Binario");
		mnArquivo.add(mnArquivoBinario);
		
		JMenuItem mntmImportar_1 = new JMenuItem("Importar");
		mnArquivoBinario.add(mntmImportar_1);
		
		JMenuItem mntmExportar_1 = new JMenuItem("Exportar");
		mnArquivoBinario.add(mntmExportar_1);
		
		JMenu mnAluno = new JMenu("Aluno");
		menuBar.add(mnAluno);
		
		JMenuItem mntmCriarGradeEscolar = new JMenuItem("Criar Grade ");
		mnAluno.add(mntmCriarGradeEscolar);
		
		JMenuItem menuItem = new JMenuItem("");
		menuBar.add(menuItem);
	}
	
	private File escolherArquivo() {
		JFileChooser fc = new JFileChooser();
		fc.setCurrentDirectory(new File(System.getProperty("user.home")));
		FileNameExtensionFilter textFilter = new FileNameExtensionFilter("Text file", "txt");
		fc.addChoosableFileFilter(textFilter);

		int result = fc.showOpenDialog(null);
		if (result == JFileChooser.APPROVE_OPTION) {
			return fc.getSelectedFile();
		}
		return null;
	}
}
